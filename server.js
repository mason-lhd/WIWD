/**
 * Created by Sherif on 12/3/2016.
 */

var express = require('express');
var app = express();

app.use(express.static('public'));
app.get('/', function (req, res) {
  res.sendFile(__dirname + "/" + "index.html");
});

var server_port = process.env.PORT || 5000;
console.log(server_port);
var server = app.listen(server_port);