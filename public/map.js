/**
 * Created by Sherif on 12/3/2016.
 * no one cares
 */

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCQpWerFW40ShhKl3U2zB4l9WuQdXVyDiI",
  authDomain: "whereiswifidown.firebaseapp.com",
  databaseURL: "https://whereiswifidown.firebaseio.com",
  storageBucket: "",
  messagingSenderId: "475511607175"
};
firebase.initializeApp(config);

var wiwdRef = firebase.database().ref("whereiswifidown");

//when a new report gets added
wiwdRef.child('posts').on("child_added", function(data){
  //update the heatmap
  pointArray.push(new google.maps.LatLng(data.val().lat, data.val().lng));
  if(!olderThanADay(data.val().date)){
    recentPointsArray.push({position: new google.maps.LatLng(data.val().lat, data.val().lng), time: data.val().date});
  }
});

// coment

var map, heatmap;
var points;
var pointArray;
var recentPointsArray = [];
var pos;


$(document).ready(function(){
  getCurrentPosition();
});


wiwdRef.on("value", function(snapchat){
  listRecentPosts();
});

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 38.829694, lng: -77.305469},
      zoom: 18
  });
  
  getCurrentPosition();
  var marker = new google.maps.Marker({
    position: pos,
    map: map
  });

  points = [];
  pointArray = new google.maps.MVCArray(points);

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: pointArray,
    map: map
  });
  heatmap.set('radius', heatmap.get('radius') ? null : 15);

  listRecentPosts();
}

// Function to add point to map
// requires use of global variable
function getCoordinates(position){
  pos = {
    lat: position.coords.latitude,
    lng: position.coords.longitude
  };
}

function getCurrentPosition(){
  if(navigator.geolocation){
    navigator.geolocation.getCurrentPosition(getCoordinates);
  }
  else{
    alert("error with location");
  }
}

function addMarkerToMap(){
  var marker = new google.maps.Marker({
    position: pos,
    map: map
  });

  //get current timestamp
  var date = new Date();
  var dateObj = {month: date.getMonth(), day: date.getDate(), hour: date.getHours()};

  wiwdRef.child('posts').push({lat: pos.lat, lng: pos.lng, date: dateObj});

  return marker;
}

function listRecentPosts(){
  for(i=0;i<recentPointsArray.length;i++){
    var currDate = new Date();
    var localPos = "Position: " + recentPointsArray[i].position.toString();
    var timeAlive = "Age: " + (currDate.getHours() - recentPointsArray[i].time.hour);
    $(".collection ul").append('<li><a href="#" onClick="map.setCenter(recentPointsArray['+i+'].position)">' + localPos + " " + timeAlive + "<\a><\li>");
  }
}

function olderThanADay(postDate){
  if(postDate == null)
    return true;
  var currDate = new Date();
  currDay = currDate.getDate();
  dayDiff = currDay - postDate.day;
  if(dayDiff >= 1){
    return true;
  }
  else{
    return false;
  }
}
